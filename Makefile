NOW	= $(shell date +%Y-%m-%d_%H-%M-%S)
BROWSER	= firefox

REPORTS_DIR	= ./reports

MAKE_DIR	= $(REPORTS_DIR)/make

CXX	= g++
CFLAGS	+= --std=c++14 -Wall -Wextra -MMD
TARGET_NAME	= sample
TARGET_DIR	= ./bin
TARGET		= $(TARGET_DIR)/$(TARGET_NAME)
INC_DIR	= ./include
INCS		= -I$(INC_DIR) -I./dependencies
SRC_DIR		= ./src
SRC_EXT		= .cpp
OBJ_EXT	= .o
SRCS		= $(wildcard $(SRC_DIR)/*$(SRC_EXT)) 
OBJ_DIR		= ./obj
OBJS		= $(subst $(SRC_DIR),$(OBJ_DIR), $(SRCS:$(SRC_EXT)=$(OBJ_EXT)))
DEPS		= $(OBJS:$(OBJ_EXT)=.d)
LIBS		= -lglog -lgflags -lpthread
#CFLAGS	= -Waggregate-return -Wcast-align -Wcast-qual -Wconversion -Wdelete-incomplete -Wdisabled-optimization -Wdouble-promotion -Weffc++ -Wfloat-equal -Wformat=2 -Winline -Wlogical-op -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wpedantic -Wpointer-arith -Wredundant-decls -Wshadow -Wsign-promo -Wstrict-null-sentinel -Wsuggest-final-methods -Wsuggest-final-types -Wsuggest-attribute=const -Wsuggest-attribute=format -Wsuggest-attribute=noreturn -Wsuggest-attribute=pure -Wsuggest-override -Wswitch-default -Wswitch-enum -Wundef -Wunsafe-loop-optimizations -Wuseless-cast -Wzero-as-null-pointer-constant
LDFLAGS		= 

LOCALE_DIR	= ./locale
LOCALE_LANG	= ja
LOCALE_POTS		= $(subst $(SRC_DIR),$(LOCALE_DIR), $(SRCS:$(SRC_EXT)=.pot))
LOCALE_POS		= $(LOCALE_POTS:.pot=.po)
LOCALE_MOS		= $(LOCALE_POS:.po=.mo)

DSG_DIR	= $(REPORTS_DIR)/design
DESIGN_CFG	= $(DSG_DIR)/design.config
DESIGN_HTML	= design
DESIGN_LANG	= Japanese
DESIGN_LOG	= $(DSG_DIR)/warnings.log

CHK_DIR	= $(REPORTS_DIR)/check
CHK_LOG	= $(CHK_DIR)/$(NOW).xml

FMT_STYLE	= google
FMT_DIR	= $(REPORTS_DIR)/format
FMT_LOG	= $(FMT_DIR)/$(NOW).xml

MET_NAME	= metrics
MET_DIR	= $(REPORTS_DIR)/$(MET_NAME)
MET_DB	= $(MET_DIR)/$(MET_NAME).db

MEM_DIR	= $(REPORTS_DIR)/memory
MEM_LOG	= $(MEM_DIR)/$(NOW).xml

PROF_NAME	= profiler
PROF_TARGET	= $(TARGET)_$(PRO_NAME)
PROF_DIR	= $(REPORTS_DIR)/$(PROF_NAME)
PROF_LOG	= $(PROF_DIR)/$(NOW).log

TEST_TARGET	= $(TARGET)_test
TEST_SRC_EXT	= .cpp
TEST_SRC_DIR	= ./tests
TEST_OBJ_DIR	= ./obj
TEST_INCS	= -I/usr/local/include
TEST_LIBS	= -lgtest -lgmock
TEST_SRCS	= $(wildcard $(TEST_SRC_DIR)/*$(TEST_SRC_EXT)) 
TEST_OBJS	= $(subst $(TEST_SRC_DIR),$(TEST_OBJ_DIR), $(TEST_SRCS:$(TEST_SRC_EXT)=$(OBJ_EXT)))
TEST_DEPS	= $(TEST_OBJS:$(OBJ_EXT)=.d)
TEST_LOG_DIR	= $(REPORTS_DIR)/test_cases
TEST_LOG	= $(TEST_LOG_DIR)/$(NOW).xml

SAN_DIR	= $(REPORTS_DIR)/sanitizer
SAN_LOG	= $(SAN_DIR)/$(NOW).xml

COV_GCDAS	= $(OBJS:$(OBJ_EXT)=.gcda) $(TEST_OBJS:$(OBJ_EXT)=.gcda)
COV_GCNOS	= $(OBJS:$(OBJ_EXT)=.gcno) $(TEST_OBJS:$(OBJ_EXT)=.gcno)
COV_DIR	= $(REPORTS_DIR)/coverage
COV_INF	= $(COV_DIR)/lcov.info

define MKDIR
	@if [ ! -d $1 ]; then \
			echo "mkdir $1"; mkdir $1; \
	fi
endef

define REDIRECT
.PHONY: $1 $(strip $1)_
$(strip $1):
	$(call MKDIR, $(MAKE_DIR))
	make $(strip $1)_ 2>&1 | tee -a $(MAKE_DIR)/$(NOW)_$(strip $1).log
endef

define MAKE_LAST
$(strip $1).make: clean-temps clean-test-temps
	rm -f *.make
	touch $(strip $1).make
endef


$(eval $(call REDIRECT, prepare))
prepare_: ## Prepare environments
	sudo apt-get install doxygen graphviz
	sudo apt-get install gettext poedit
	sudo apt-get install clang-format cppcheck cccc
	sudo apt-get install valgrind lcov
	#wget https://github.com/google/googletest/archive/release-1.8.0.tar.gz

$(eval $(call REDIRECT, docs))
docs_: ## Generate documents
	pandoc -s -t html -c Project.css -o docs/Project.html docs/Project.md
	wkhtmltopdf -B 30mm -L 20mm -R 20mm -T 30mm --footer-center '[page]/[topage]' --footer-spacing 2 --print-media-type --header-right [date] --header-spacing 2 docs/Project.html docs/Project.pdf

$(eval $(call REDIRECT, all))
all_: BROWSER= echo
all_: clean build-release codes-all test-all ## Clean, build, and run static and dynamic tests

$(eval $(call REDIRECT, run))
run_: $(TARGET) ## Run the target
	env LANG= $<

$(eval $(call REDIRECT, build-debug))
build-debug_: CFLAGS+= -O0 -g
build-debug_: $(TARGET) ## Build the debug binary file
$(eval $(call REDIRECT, build-release))
build-release_: CFLAGS+= -O2
build-release_: $(TARGET) ## Build the release binary file
$(eval $(call REDIRECT, build-fast))
build-fast_: CFLAGS+= -O3 -march=native -funroll-loops -fomit-frame-pointer -fomg-optimize -frerun-loop-opt -finline-functions -fforce-addr -fforce-mem
build-fast_: $(TARGET) ## Build the fast binary file

$(TARGET): $(OBJS) $(LIBS)
	$(call MKDIR, $(TARGET_DIR))
	$(CXX) -o $@ $(OBJS) $(LDFLAGS) $(LIBS)

$(OBJ_DIR)/%$(OBJ_EXT): $(SRC_DIR)/%$(SRC_EXT)
	$(call MKDIR, $(OBJ_DIR))
	$(CXX) -c $(CFLAGS) $(INCS) $< -o $@

-include $(DEPS)

$(eval $(call REDIRECT, locale-init))
locale-init_: $(LOCALE_POS) ## Generate files for localization

$(eval $(call REDIRECT, locale-compile))
locale-compile_: $(LOCALE_MOS) ## Compile the edited files for localization

$(LOCALE_DIR)/%.po: $(LOCALE_DIR)/%.pot
	-msginit --locale=$(LOCALE_LANG) --input=$< --output=$@ --no-translator
	-grep -l 'charset=ASCII' $@ | xargs sed -i -e 's/charset=ASCII/charset=utf-8/g'

$(LOCALE_DIR)/%.pot: $(SRC_DIR)/%$(SRC_EXT)
	-xgettext -k"_" -o $@ $<

$(LOCALE_DIR)/%.mo: $(LOCALE_POS)
	msgfmt --output=$@ $<
	mv $@ $(LOCALE_DIR)/$(LOCALE_LANG)/LC_MESSAGES/

##### STATIC TESTS #####

$(eval $(call REDIRECT, codes-all))
codes-all_: codes-design codes-check codes-format codes-metrics ## Run static tests with reports

$(eval $(call REDIRECT, codes-design))
codes-design_: $(DESIGN_CFG) $(DESIGN_HTML) ## Generate documents

$(DESIGN_CFG):
	$(call MKDIR, $(DSG_DIR))
	doxygen -g $(DESIGN_CFG)
	@sed -i -E 's/^(PROJECT_NAME *= ").*"$$/\1$(TARGET_NAME)"/g' $(DESIGN_CFG)
	@sed -i -E 's/^OUTPUT_DIRECTORY *=$$/& $(subst /,\/,$(REPORTS_DIR))/g' $(DESIGN_CFG)
	@sed -i -E 's/^(OUTPUT_LANGUAGE *=).*$$/\1 $(DESIGN_LANG)/g' $(DESIGN_CFG)
	@sed -i -E 's/^(BUILTIN_STL_SUPPORT *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(EXTRACT_ANON_NSPACES *=) NO$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(QUIET *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(WARN_NO_PARAMDOC *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(WARN_LOGFILE *=).*$$/\1 $(subst /,\/,$(DESIGN_LOG))/g' $(DESIGN_CFG)
	@sed -i -E 's/^INPUT *=$$/& $(subst /,\/,$(INC_DIR)) $(subst /,\/,$(SRC_DIR))/g' $(DESIGN_CFG)
	@sed -i -E 's/^(SOURCE_BROWSER *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(INLINE_SOURCES *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(REFERENCED_BY_RELATION *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(REFERENCES_RELATION *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(HTML_OUTPUT *=).*$$/\1 $(DESIGN_HTML)/g' $(DESIGN_CFG)
	@sed -i -E 's/^(HTML_TIMESTAMP *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(HTML_DYNAMIC_SECTIONS *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(DISABLE_INDEX *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(GENERATE_TREEVIEW *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(GENERATE_LATEX *=).*$$/\1 NO/g' $(DESIGN_CFG)
	@sed -i -E 's/^(UML_LOOK *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(CALL_GRAPH *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(CALLER_GRAPH *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(INTERACTIVE_SVG *=).*$$/\1 YES/g' $(DESIGN_CFG)
	@sed -i -E 's/^(DOT_MULTI_TARGETS *=).*$$/\1 YES/g' $(DESIGN_CFG)

$(DESIGN_HTML):
	doxygen $(DESIGN_CFG)
	$(BROWSER) $(REPORTS_DIR)/$(DESIGN_HTML)/index.html

$(eval $(call REDIRECT, codes-check))
codes-check_: $(SRCS) ## Check potential errors in the source codes
	$(call MKDIR, $(CHK_DIR))
	cppcheck --enable=all -I$(INC_DIR) --xml --std=c++11 $^ 2> $(CHK_LOG)

$(eval $(call REDIRECT, codes-format))
codes-format_: ## Format all of the source codes
	$(call MKDIR, $(FMT_DIR))
	clang-format -i -style=$(FMT_STYLE) $(SRC_DIR)/*$(SRC_EXT) -output-replacements-xml > $(FMT_LOG)

$(eval $(call REDIRECT, codes-metrics))
codes-metrics_: ## Measure metrics
	cccc --outdir=$(MET_DIR) --html_outfile=$(MET_DIR)/index.html --db_infile=$(MET_DB) --db_outfile=$(MET_DB) $(SRCS)

##### DYNAMIC TESTS #####

$(eval $(call REDIRECT, test-all))
test-all_: test test-profile test-memory ## Run dynamic tests with a report

$(eval $(call REDIRECT, test))
test_: CFLAGS+= --coverage -DNOLOG -g -O0
test_: LDFLAGS+= --coverage
test_: $(TEST_TARGET) ## Run tests with a report
	-$(TEST_TARGET) --gtest_output="xml:$(TEST_LOG)" 2>&1 | tee -a $(SAN_LOG)
	sed -i -E "s/<\?.*\?>/&\n<?xml-stylesheet type=\"text\/xsl\" href=\"test_cases.xsl\" ?>/" $(TEST_LOG)
	#$(BROWSER) $(TEST_LOG) &
	echo 'genhtml_branch_coverage = 1' > ~/.lcovrc
	echo 'lcov_branch_coverage = 1' >> ~/.lcovrc
	#$(RM) $(COV_GCDAS)
	cd $(OBJ_DIR)
	gcov -bc test_main$(OBJ_EXT)
	cd ..
	@if [ ! -d $(COV_DIR) ]; then \
			echo ";; mkdir $(COV_DIR)"; mkdir $(COV_DIR); \
	fi
	lcov --quiet --capture -directory $(OBJ_DIR) -o $(COV_INF)
	lcov --quiet -e $(COV_INF) \*$(subst .,,$(SRC_DIR))\* -o $(COV_INF)
	genhtml --quiet --frames -o $(COV_DIR) $(COV_INF)
	lcov -l $(COV_INF)
	#$(BROWSER) $(COV_DIR)/index.html &

$(eval $(call REDIRECT, test-profile))
test-profile_: CFLAGS+= -pg
test-profile_: LDFLAGS+= -pg
test-profile_: clean-temps clean-test-temps $(PROF_TARGET) ## Check the performance of tests
	$(call MKDIR, $(PROF_DIR))
	-$<
	gprof $< gmon.out > $(PROF_LOG)
	$(RM) gmon.out
	vim $(PROF_LOG)

$(PROF_TARGET): $(OBJS)
	$(call MKDIR, $(TARGET_DIR))
	$(CXX) -o $@ $(OBJS) $(LDFLAGS) $(LIBS)

$(eval $(call REDIRECT, test-memory))
test-memory_: clean-temps clean-test-temps $(TEST_TARGET) ## Check memory leaks while running tests
	$(call MKDIR, $(MEM_DIR))
	-valgrind --leak-check=full --xml=yes --xml-file=$(MEM_LOG) $(TEST_TARGET)

$(TEST_TARGET): CFLAGS+= -fsanitize=address -fsanitize=leak
$(TEST_TARGET): LDFLAGS+= -fsanitize=address -fsanitize=leak
$(TEST_TARGET): $(OBJS) $(TEST_OBJS)
	$(call MKDIR, $(TARGET_DIR))
	$(CXX) -o $@ $(filter-out %/main$(OBJ_EXT),$(OBJS)) $(TEST_OBJS) $(LIBS) $(TEST_LIBS) $(LDFLAGS)

$(TEST_OBJ_DIR)/%$(OBJ_EXT): $(TEST_SRC_DIR)/%$(TEST_SRC_EXT)
	$(CXX) -c $(CFLAGS) $(INCS) $(TEST_INCS) $< -o $@

-include $(TEST_DEPS)

$(eval $(call REDIRECT, clean-all))
clean-all_: clean ## Clean all objects including localization
	$(RM) $(LOCALE_POS)

.PHONY: clean _clean
clean: _clean
clean: clean-test clean-temps clean-locale clean-design ## Clean the output objects
	$(RM) $(TARGET)
	-$(RM) -d $(TARGET_DIR)
	-$(RM) -d $(OBJ_DIR)

.PHONY: clean-temps
clean-temps:
	$(RM) $(OBJS) $(DEPS)

.PHONY: clean-locale
clean-locale: ## Clean locale files
	$(RM) $(LOCALE_POTS) $(LOCALE_MOS)

.PHONY: clean-design
clean-design: ## Clean document files
	$(RM) $(DESIGN_CFG)
	$(RM) -r $(REPORTS_DIR)/$(DESIGN_HTML)

.PHONY: clean-test
clean-test: clean-test-temps clean-test-cov
	$(RM) $(TEST_TARGET) $(PROF_TARGET)

.PHONY: clean-test-temps
clean-test-temps:
	$(RM) $(TEST_OBJS) $(TEST_DEPS)

.PHONY: clean-test-cov
clean-test-cov:
	$(RM) $(COV_GCDAS) $(COV_GCNOS) $(COV_INF)
	$(RM) -r $(COV_DIR)

.DEFAULT_GOAL := help
.PHONY: help
help: ## Show help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | sed -E 's/^[^:]*:([^ ])/\1/g' | sed -E 's/^(.*[^_])_{0,1}:(.*)/\1:\2/g' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'


if g:dein#_cache_version != 100 | throw 'Cache loading error' | endif
let [plugins, ftplugin] = dein#load_cache_raw(['/home/set/.vimrc'])
if empty(plugins) | throw 'Cache loading error' | endif
let g:dein#_plugins = plugins
let g:dein#_ftplugin = ftplugin
let g:dein#_base_path = '/home/set/.vim/dein'
let g:dein#_runtime_path = '/home/set/.vim/dein/.cache/.vimrc/.dein'
let g:dein#_cache_path = '/home/set/.vim/dein/.cache/.vimrc'
let &runtimepath = '/home/set/.vim,/var/lib/vim/addons,/home/set/.vim/dein/.cache/.vimrc/.dein,/usr/share/vim/vimfiles,/usr/share/vim/vim74,/usr/share/vim/vimfiles/after,/var/lib/vim/addons/after,/home/set/.vim/after,/home/set/.vim/bundles/repos/github.com/Shougo/dein.vim,/home/set/.vim/dein/.cache/.vimrc/.dein/after'
filetype off
silent! command -complete=customlist,dein#autoload#_dummy_complete -bang -bar -range -nargs=* NERDTreeToggle call dein#autoload#_on_cmd('NERDTreeToggle', 'nerdtree', <q-args>,  expand('<bang>'), expand('<line1>'), expand('<line2>'))
autocmd dein-events InsertEnter * call dein#autoload#_on_event("InsertEnter", ['deoplete.nvim'])

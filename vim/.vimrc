if &compatible
  set nocompatible
endif
set runtimepath+=~/.vim/bundles/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.vim/dein')
  call dein#begin('~/.vim/dein')

  call dein#add('Shougo/dein.vim')
  call dein#add('Shougo/unite.vim')
  call dein#add('Shougo/neocomplete.vim')
  call dein#add('Shougo/neosnippet')
  call dein#add('Shougo/neoinclude.vim')

  " lazy load on filetype
  call dein#add('justmao945/vim-clang',
        \{'on_ft': ['c', 'cpp']})

  let g:clang_c_options = '-std=c11'
  let g:clang_cpp_options = '-std=c++11'
  let g:clang_check_syntax_auto = 1
  let g:clang_format_auto = 1
  let g:clang_format_style = 'Google'

  " lazy load on command executed
  call dein#add('scrooloose/nerdtree',
        \{'on_cmd': 'NERDTreeToggle'})
  " lazy load on insert mode
  call dein#add('Shougo/deoplete.nvim',
        \{'on_i': 1})
  " lazy load on function call
  call dein#add('othree/eregex.vim',
        \{'on_func': 'eregex#toggle'})
 
  " disable auto completion for vim-clang
  let g:clang_auto = 0
  " default 'longest' can not work with neocomplete
  let g:clang_c_completeopt = 'menuone,preview'
  let g:clang_cpp_completeopt = 'menuone,preview'

  " use neocomplete
  " input patterns
  if !exists('g:neocomplete#force_omni_input_patterns')
    let g:neocomplete#force_omni_input_patterns = {}
  endif

  " for c and c++
  let g:neocomplete#force_omni_input_patterns.c =
        \ '[^.[:digit:] *\t]\%(\.\|->\)\w*'
  let g:neocomplete#force_omni_input_patterns.cpp =
        \ '[^.[:digit:] *\t]\%(\.\|->\)\w*\|\h\w*::\w*'

  let g:neocomplete#enable_at_startup = 1
  let g:neocomplete#include_paths = {
    \ 'cpp' : '/home/set/template/include/',
    \ }
  let g:neocomplete#include_patterns = {
\ 'cpp' : '^\s*#\s*include',
\ }
 call dein#end()
  call dein#save_state()
endif

filetype plugin indent on
syntax enable

" setting
"文字コードをUFT-8に設定
set fenc=utf-8
" バックアップファイルを作らない
set nobackup
" スワップファイルを作らない
set noswapfile
" 編集中のファイルが変更されたら自動で読み直す
set autoread
" バッファが編集中でもその他のファイルを開けるように
set hidden
" 入力中のコマンドをステータスに表示する
set showcmd
set mouse=a

" 見た目系
" 行番号を表示
set number
" 現在の行を強調表示
set cursorline
" 現在の行を強調表示（縦）
set cursorcolumn
" 行末の1文字先までカーソルを移動できるように
set virtualedit=onemore
" インデントはスマートインデント
set smartindent
" ビープ音を可視化
set visualbell
" 括弧入力時の対応する括弧を表示
set showmatch
" ステータスラインを常に表示
set laststatus=2
" コマンドラインの補完
set wildmode=list:longest
" 折り返し時に表示行単位での移動できるようにする
nnoremap j gj
nnoremap k gk


" Tab系
" 不可視文字を可視化(タブが「▸-」と表示される)
set list listchars=tab:\▸\-
" 行頭以外のTab文字の表示幅（スペースいくつ分）
set tabstop=2
" 行頭でのTab文字の表示幅
set shiftwidth=2
set expandtab


" 検索系
" 検索文字列が小文字の場合は大文字小文字を区別なく検索する
set ignorecase
" 検索文字列に大文字が含まれている場合は区別して検索する
set smartcase
" 検索文字列入力時に順次対象文字列にヒットさせる
set incsearch
" 検索時に最後まで行ったら最初に戻る
set wrapscan
" 検索語をハイライト表示
set hlsearch
" ESC連打でハイライト解除
nmap <Esc><Esc> :nohlsearch<CR><Esc>


#include "client_main.h"

#include <chrono>
#include <iostream>

#include "thread_utils.h"
#include "socket_client.h"
#include "user_communicator.h"

ClientMain::ClientMain(int argc, char *argv[]) { parseArgs(argc, argv); }

ClientMain::~ClientMain() {}

void ClientMain::run() {
  std::shared_ptr<UserCommunicator> user_communicator;
  SocketClient socket_client;
  std::shared_ptr<Channel<std::string>> request_channel{
      new Channel<std::string>{}};
  std::shared_ptr<Channel<std::string>> response_channel{
      new Channel<std::string>{}};

  user_communicator->setInputChannel(response_channel);
  user_communicator->setOutputChannel(request_channel);
  socket_client.setInputChannel(request_channel);
  socket_client.setInputChannel(response_channel);
  std::cout << "Searching MFP..." << std::flush;
  while (!socket_client.init(host, port)) {
    std::this_thread::sleep_for(std::chrono::seconds(1));
  };
  std::cout << "found" << std::endl;

  auto input_processor = createForeverThread(&UserCommunicator::processInput, user_communicator);
  auto output_printer = createForeverThread(&UserCommunicator::printOutput, user_communicator);
  auto request_sender = createForeverThread(&SocketClient::send, &socket_client);
  auto response_receiver = createForeverThread(&SocketClient::receive, &socket_client);

  input_processor.join();
  request_sender.join();
  response_receiver.join();
  output_printer.join();
}

void ClientMain::parseArgs(int argc, char *argv[]) {
  if (argc != 3) {
    return;
  }
  host = argv[1];
  port = argv[2];
}

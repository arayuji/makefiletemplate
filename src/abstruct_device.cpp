#include "abstruct_device.h"

DeviceStatus::DeviceStatus(DeviceState device_state, int page_number) : device_state_(device_state), page_number_(page_number) {}

AbstructDevice::AbstructDevice(std::shared_ptr<Channel<DeviceStatus>> channel) {
  channel_ = channel;
}

void AbstructDevice::run(int page_number, std::ios *) {
  std::unique_lock<std::mutex> lock(mutex_);
  for (int i = 0; i < page_number; i++) {
    if (condition_canceled_.wait_for(lock, std::chrono::seconds(duration),
          [&] { return canceled_; })) {
      channel_->enqueue(DeviceStatus(DeviceState::Canceled));
      return;
    }
    channel_->enqueue(DeviceStatus(DeviceState::Executing, i));
  }
  channel_->enqueue(DeviceStatus(DeviceState::Completed));
}

void AbstructDevice::cancel() {
  std::unique_lock<std::mutex> lock(mutex_);
  canceled_ = true;
  condition_canceled_.notify_all();
}


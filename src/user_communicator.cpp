#include "user_communicator.h"

#include <iostream>

UserCommunicator::UserCommunicator() {}

UserCommunicator::~UserCommunicator() {}

bool UserCommunicator::processInput() {
  std::string input_string;
  std::cout << "Please input operation..." << std::endl;
  std::getline(std::cin, input_string);
  output_channel_->enqueue(input_string);
  return true;
}

bool UserCommunicator::printOutput() {
  const auto message = input_channel_->dequeue();
  std::cout << message << std::endl;
  return true;
}

void UserCommunicator::setInputChannel(std::shared_ptr<Channel<std::string>> input_channel) {
  input_channel_ = input_channel;
}

void UserCommunicator::setOutputChannel(std::shared_ptr<Channel<std::string>> output_channel) {
  output_channel_ = output_channel;
}

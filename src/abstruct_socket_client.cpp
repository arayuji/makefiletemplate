#include "abstruct_socket_client.h"

#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>

AbstructSocketClient::~AbstructSocketClient() {
  if (0 <= server_sock_) {
    close(server_sock_);
    server_sock_ = kNullSocket;
  }
}

bool AbstructSocketClient::init(const std::string &ip_address, const std::string &service_name) {
  struct addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_family = PF_UNSPEC;
  
  struct addrinfo *results;
  int err;
  if ((err = getaddrinfo(ip_address.c_str(), service_name.c_str(), &hints, &results)) != 0) {
    SocketException((std::string)"getaddrinfo: " + gai_strerror(err));
    return false;
  }

  struct addrinfo *result;
  for (result = results; result != nullptr; result = result->ai_next) {
    server_sock_ = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (server_sock_ < 0) {
      continue;
    }
    if (connect(server_sock_, result->ai_addr, result->ai_addrlen) != 0) {
      close(server_sock_);
      continue;
    }
    break;
  }
  freeaddrinfo(results);
  
  if (result == nullptr) {
    SocketException("connect: No valid server");
    return false;
  }

  return true;
}


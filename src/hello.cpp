#include "hello.h"
#include <iostream>
#include <libintl.h>
#define _(String) gettext(String)
#define N_(String) gettext_noop(String)
#define gettext_noop(String) (String)

Hello::Hello(const std::string &string) : m_string(string) {}

bool Hello::say() {
  LOG(INFO) << "This is the beginning of " << __func__;
  std::cout << "Hello World! : " << m_string << std::endl;
  LOG(ERROR) << "This is the beginning of Hello::say()";
  return !m_string.empty();
}

bool Hello::say(const You &you) const {
  return you.say(m_string);
}

const char *Hello::tell() { return m_string.c_str(); }

void Hello::talk() {}

std::string Hello::listen() {
  std::string str;
  std::cin >> str;
  return str;
}


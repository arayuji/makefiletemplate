#include "socket_client.h"

#include <iostream>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

SocketClient::SocketClient() {}

SocketClient::~SocketClient() {
  if (0 <= server_socket_) {
    close(server_socket_);
    server_socket_ = NULL_SOCKET;
  }
}

bool SocketClient::init(const std::string &ip_address,
                        const std::string &service_name) {
  struct addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_family = PF_UNSPEC;

  struct addrinfo *results;
  int err;
  if ((err = getaddrinfo(ip_address.c_str(), service_name.c_str(), &hints,
                         &results)) != 0) {
    SocketException((std::string) "getaddrinfo: " + gai_strerror(err));
    return false;
  }

  struct addrinfo *result;
  for (result = results; result != nullptr; result = result->ai_next) {
    server_socket_ =
        socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (server_socket_ < 0) {
      continue;
    }
    if (connect(server_socket_, result->ai_addr, result->ai_addrlen) != 0) {
      close(server_socket_);
      continue;
    }
    break;
  }
  freeaddrinfo(results);

  if (result == nullptr) {
    SocketException("connect: No valid server");
    return false;
  }

  return true;
}

bool SocketClient::send() {
  try {
    const auto message = input_channel_->dequeue();
    writeString(server_socket_, message);
    return true;
  } catch (...) {
    return false;
  }
}

bool SocketClient::receive() {
  try {
    auto message = readString(server_socket_);
    output_channel_->enqueue(message);
    return true;
  } catch (...) {
    return false;
  }
}

std::string SocketClient::readString(int fd) {
  std::string string;
  char buf[2] = {0};
  while (0 < read(fd, buf, 1)) {
    if (buf[0] == '\n') {
      break;
    }
    string += buf[0];
  }
  return string;
}

void SocketClient::writeString(int fd, const std::string &string) {
  if (write(fd, string.c_str(), string.size())) {
    std::cerr << "Error in writing" << std::endl;
  }
}

void SocketClient::setInputChannel(std::shared_ptr<Channel<std::string>> input_channel) {
  input_channel_ = input_channel;
}

void SocketClient::setOutputChannel(std::shared_ptr<Channel<std::string>> output_channel) {
  output_channel_ = output_channel;
}

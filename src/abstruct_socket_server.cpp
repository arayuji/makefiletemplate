#include "abstruct_socket_server.h"

#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>

AbstructSocketServer::~AbstructSocketServer() {
  if (0 <= server_sock_) {
    close(server_sock_);
    server_sock_ = kNullSocket;
  }
}

bool AbstructSocketServer::init(const std::string &service_name, int max_connections) {
  struct addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_flags = AI_PASSIVE;
  hints.ai_socktype = SOCK_STREAM;

  //名前解決をする
  struct addrinfo *result;
  int retval = getaddrinfo(NULL, service_name.c_str(), &hints, &result);
  if (retval != 0) {
    throw SocketException((std::string)"getaddrinfo : " + gai_strerror(retval));
    return false;
  }

  //ソケットを作成する
  server_sock_ = socket(result->ai_family, result->ai_socktype, 0);
  if (server_sock_ < 0) {
    throw SocketException((std::string)"socket : " + strerror(errno));
    return false;
  }

  //ソケットにアドレスを割り当てる
  if (bind(server_sock_, result->ai_addr, result->ai_addrlen) != 0) {
    throw SocketException((std::string)"bind : " + strerror(errno));
    return false;
  }
  freeaddrinfo(result);

  /* 接続要求を待てる状態にする */
  listen(server_sock_, max_connections);

  return true;
}


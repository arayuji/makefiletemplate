#include <gflags/gflags.h>
#include <libintl.h>

#include "client_main.h"
#include "hello.h"

#define _(String) gettext(String)
#define N_(String) gettext_noop(String)
#define gettext_noop(String) (String)

/*!
\brief The start point
\param none
\return Whether success
*/
int main(int argc, char *argv[]) {
  setlocale(LC_ALL, "");
  bindtextdomain("default", "locale");
  textdomain("default");

  google::ParseCommandLineFlags(&argc, &argv, true);
#ifndef NOLOG
  google::InitGoogleLogging(argv[0]);
  google::InstallFailureSignalHandler();
#endif
  Hello(_("Main")).say();
  setlocale(LC_ALL, "ja_JP.UTF8");
  Hello hello(_("Main"));
  hello.say();

  try {
    ClientMain client_main{argc, argv};
    client_main.run();
  } catch (...) {
  }

  return 0;
}

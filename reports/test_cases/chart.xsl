<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" encoding="UTF-8" />
<xsl:template match="/">
<html>
<head>
	<title>Test cases</title>
</head>
<body>
	<table>
		<tr>
			<th>Suite</th>
			<th>Case</th>
			<th>Input</th>
			<th>Expection</th>
			<th>Result</th>
		</tr>
		<xsl:for-each select="testsuites/testsuite">
		<xsl:for-each select="testcase">
		<tr>
			<td><xsl:value-of select="../@name" /></td>
			<td><xsl:value-of select="@name" /></td>
			<td><xsl:value-of select="@input" /></td>
			<td><xsl:value-of select="@output" /></td>
			<td><xsl:value-of select="failure/@message" /></td>
		</tr>
		</xsl:for-each>
		</xsl:for-each>
	</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>


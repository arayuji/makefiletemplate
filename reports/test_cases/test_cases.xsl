<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="UTF-8" />
  <xsl:template match="/">
    <html>
      <head>
        <title>Test cases</title>
        <link rel="stylesheet" type="text/css" href="test_cases.css" />
      </head>
      <body>
        <table>
          <tr>
            <th>Suite</th>
            <th>Case</th>
            <th>Input</th>
            <th>Expection</th>
            <th>Result</th>
          </tr>
          <xsl:for-each select="testsuites/testsuite">
            <xsl:for-each select="testcase">
              <tr>
                <td><xsl:value-of select="../@name" /></td>
                <td><xsl:value-of select="@name" /></td>
                <td><pre><xsl:call-template name="replace">
                      <xsl:with-param name="str" select="@input" />
                    </xsl:call-template></pre></td>
                <td><pre><xsl:value-of select="@output" /></pre></td>
                <xsl:choose>
                  <xsl:when test="string-length(failure/@message)=0">
                    <td>OK</td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td>NG</td>
                  </xsl:otherwise>
                </xsl:choose>
              </tr>
            </xsl:for-each>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="replace">
    <xsl:param name="str" />

    <xsl:choose>
      <xsl:when test="contains($str, ';')">
        <xsl:value-of select="substring-before($str, ';')" />;<br />
        <xsl:call-template name="replace">
          <xsl:with-param name="str" select="substring(substring-after($str, ';'), 2)" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$str" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>


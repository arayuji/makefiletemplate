"use strict";

// -------------------------------------------------
// 初期設定（いったんHTMLを空にする）
// -------------------------------------------------
$(function(){
		$("table.tbl tbody").html("");
		});

var test_labels = [];
var tests = [];
var failures = [];
var data = [[], [], []];
var count = 0;
var mydata = {
labels: test_labels,
datasets: [{
label: 'Tests',
hoverBackgroundColor: 'rgba(255,99,132,0.3)',
data: tests
}]
};

var options = {
	bezierCurve : false,
	animation : true,
title: {
display: true,
text: 'Tests'
}
};

// -------------------------------------------------
// XML読み込み
// -------------------------------------------------
var chart;
function loadXml(xml_url){
	$.ajax({
		url: xml_url,
		type: 'get',
		dataType: 'xml',
		timeout: 1000,
		success:function(xml, status) {
			$(xml).find('testsuites').each(disp);
			chart = new Chart($('#stage'), {
type: 'line',
data: mydata,
options: options
});
		}
	});
}

// -------------------------------------------------
// HTML生成関数
// -------------------------------------------------

function disp(){
	test_labels[count] = $(this).attr('timestamp');
	tests[count] = $(this).attr('tests');
	failures[count] = $(this).attr('failures');
	data[count] = [$(this).attr('timestamp'), $(this).attr('tests'), $(this).attr('failures')];
	$('span#status').text('test=' + tests[count]);

	//HTMLを生成
	$('<tr>'+
			'<td>'+tests[count]+'</td>'+
			'<td>'+failures[count]+'</td>'+
			'</tr>').appendTo('table.tbl tbody');

	count++;
}

var readers = [];
$(".dnd").on("drop",function(e){
	e.preventDefault();
	var files = e.originalEvent.dataTransfer.files;
	for(var i = 0; i < files.length; i++){
		readers[i] = new FileReader();
		readers[i].addEventListener('load', function() {
			loadXml(this.result);
		});
		readers[i].readAsDataURL(files[i]);
	}
});
$(".dnd").on("dragover",function(e){
	e.preventDefault();
});

//関数実行
$(function(){
		$('span#status').text('loading');
});


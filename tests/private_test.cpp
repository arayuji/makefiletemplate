// privateメンバへのアクセス サンプル

// 元となるコード
// https://g...content-available-to-author-only...b.com/1528856
// Accessing Private Data
// c.f.
// http://b...content-available-to-author-only...t.com/2010/07/access-to-private-members-thats-easy.html

#include "hello.h"
#include "test_utils.h"
#include <iostream>

class HelloClassTest : public TestTemplate {};

REVEAL(Hello, tell);

namespace {
TEST_F(HelloClassTest, Tell) {
  TEST_IN(const char *TEST_STRING = "TEST"; Hello hello(TEST_STRING);)
  TEST_OUT(EXPECT_STREQ, TEST_STRING, ACCESS(hello, Hello, tell)());
}
}

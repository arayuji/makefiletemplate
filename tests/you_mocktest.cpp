#include "you.h"

#include "test_utils.h"

using ::testing::Mock;
using ::testing::_;

namespace {

  class YouTest : public TestTemplate {
  };

  TEST_F(YouTest, say) {
    TEST_IN(
        std::string str = "You";
        std::unique_ptr<You> you_p{new You{}};
        );
    TEST_CALL(
        EXPECT_CALL(*you_p, say(_))
        .Times(2);
        );
    TEST_IN(
      you_p->say(str);
      you_p->say(str);
      you_p->say();
      );
  }

}


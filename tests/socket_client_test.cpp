#include "socket_client.h"

#include "test_utils.h"
#include "gtest/gtest.h"
#include <fcntl.h>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>

REVEAL(SocketClient, server_socket_);

namespace {

class SocketClientTest : public TestTemplate {
public:
  SocketClientTest()
      : socket_client_{std::unique_ptr<SocketClient>{new SocketClient{}}},
        input_channel_(
            std::shared_ptr<Channel<std::string>>{new Channel<std::string>}),
        output_channel_(
            std::shared_ptr<Channel<std::string>>{new Channel<std::string>}) {
    socket_client_->setInputChannel(input_channel_);
    socket_client_->setOutputChannel(output_channel_);
  }
  ~SocketClientTest() {
    socket_client_.reset();
    input_channel_.reset();
    output_channel_.reset();
  }

protected:
  std::string TEST_STRING = "test";
  std::unique_ptr<SocketClient> socket_client_;
  std::shared_ptr<Channel<std::string>> input_channel_;
  std::shared_ptr<Channel<std::string>> output_channel_;
};

TEST_F(SocketClientTest, send_Pass) {
  int file = open("./hui", O_WRONLY | O_CREAT|O_TRUNC);
  if (write(file, "yuet", 4) < 0) {
    std::cerr << "ixhew" << std::endl;
  }
  //ACCESS_P(socket_client_.get(), SocketClient, server_socket_) = _file;
  //TEST_IN(input_channel_->enqueue("test\n"); socket_client_->send();)
  //ACCESS_P(socket_client_.get(), SocketClient, server_socket_) = -1;
  if (close(file) < 0) {
    std::cerr << "ijew" << std::endl;
  }
/*  std::ifstream _ifs("outtemp");
  EXPECT_EQ(std::string((std::istreambuf_iterator<char>(_ifs)),
                        std::istreambuf_iterator<char>()),
            TEST_STRING);
  _ifs.close();*/
}

TEST_F(SocketClientTest, receive_Pass) {
  std::ofstream _ofs("temp");
  _ofs << TEST_STRING << "\n" << std::endl;
  _ofs.close();
  int _file = open("temp", O_RDWR | O_CREAT);
  ACCESS_P(socket_client_.get(), SocketClient, server_socket_) = _file;

  TEST_IN(socket_client_->receive();)
  TEST_OUT(EXPECT_EQ, TEST_STRING, output_channel_->dequeue());

  close(_file);
}

} // namespace

#include <iostream>
#include <sstream>

#include "gtest/gtest.h"

class StdinTest {
  public:
    StdinTest(std::string input) {
      sstream.str(input);
      stdbuffer = std::cin.rdbuf(sstream.rdbuf());
    }
    ~StdinTest() {
      std::cin.rdbuf(stdbuffer);
    }
  private:
    std::istringstream sstream;
    std::streambuf *stdbuffer;
};

class StdoutTest {
  public:
    StdoutTest() {
      stdbuffer = std::cout.rdbuf(&buffer);
    }
    ~StdoutTest() {
      std::cout.rdbuf(stdbuffer);
    }
    std::string text() const {
      return buffer.str();
    }
  private:
    std::stringbuf buffer;
    std::streambuf *stdbuffer;
};

#define JA_EXPECT_EQ "と等しい";
#define JA_EXPECT_STREQ "と等しい";
#define JA_EXPECT_NE "と等しくない";
#define JA_EXPECT_LT "より大きい";
#define JA_EXPECT_LE "以上";
#define JA_EXPECT_GT "より小さい";
#define JA_EXPECT_GE "以下";

#define JA_ASSERT_EQ "と等しい";
#define JA_ASSERT_NE "と等しくない";
#define JA_ASSERT_LT "より大きい";
#define JA_ASSERT_LE "以上";
#define JA_ASSERT_GT "より小さい";
#define JA_ASSERT_GE "以下";

#define TEST_STDOUT_START() \
  testing::internal::CaptureStdout();

#define TEST_STDIN(input) \
  StdinTest stdin_test(input); \

#define TEST_IN(operations) \
  RecordProperty("input", #operations); \
operations;

#define TEST_CALL(macro) \
  _output += #macro; \
macro;

#define TEST_RET(macro, expectation, operation) \
  _output += "返り値が" #expectation JA_##macro; \
_output += "\n"; \
macro(expectation, operation);

#define TEST_OUT(macro,expectation,value) \
  _output += #value "が" #expectation JA_##macro; \
_output += "\n"; \
macro(expectation, value);

#define TEST_STDOUT_END(expectation) \
  _output += "標準出力が" #expectation "と等しい\n"; \
EXPECT_EQ(expectation, testing::internal::GetCapturedStdout());

namespace {

class TestTemplate : public ::testing::Test {
  public:
    virtual ~TestTemplate() {
      RecordProperty("output", _output);
    }
  protected:
    std::string _output;
};

}

// Tag::type はアクセスするメンバのポインタ型
template <class Tag>
struct Accessor {
  static typename Tag::type value;
};

// staticメンバの実体
template <class Tag>
typename Tag::type Accessor<Tag>::value;

template <class Tag, typename Tag::type p>
struct Initializer {
  // Accessor型のstaticメンバ value に p (メンバポインタ) 
  Initializer() { Accessor<Tag>::value = p; }
  static Initializer instance;
};

// 初期化を駆動するための static オブジェクト(自身)の定義
// explicit instantiation によって、int main() 前に
// 生成されることが確定し、生成の際、コンストラクタが
// 呼び出されることになる。
template <class Tag, typename Tag::type p>
Initializer<Tag, p> Initializer<Tag, p>::instance;

template<class Type>
struct Func { typedef Type type; };

// Tagクラス。アクセスするメンバの、メンバポインタ型を
// をタイプメンバ type として持つ
#define REVEAL(Class, Member) \
  typedef struct Func<decltype(&Class::Member)> Class ## _ ## Member; \
template struct Initializer<Class ## _ ## Member, &Class::Member>;

#define ACCESS(Instance, Class, Member) \
  (Instance.*Accessor<Class ## _ ## Member>::value)

#define ACCESS_P(Instance, Class, Member) \
  (Instance->*Accessor<Class ## _ ## Member>::value)


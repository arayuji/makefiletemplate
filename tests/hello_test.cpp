#include "you.h"
#include "hello.h"
#include "test_utils.h"

REVEAL(Hello, tell);

namespace {

class HelloTest : public TestTemplate {
protected:
  const std::string TEST_STRING = "test";
  const std::string EMPTY_STRING = "";
};

/*!
  \test {
  Hello class returns true for non-empty input.
  }
  */
TEST_F(HelloTest, True) {
  TEST_STDOUT_START();
  TEST_IN(Hello hello(TEST_STRING););
  TEST_RET(EXPECT_EQ, true, hello.say());
  TEST_OUT(EXPECT_EQ, 1 + 1, 2);
  TEST_STDOUT_END("Hello World! : test\n");
}

TEST_F(HelloTest, False) {
  TEST_STDOUT_START();
  TEST_IN(Hello hello(EMPTY_STRING););
  TEST_RET(EXPECT_EQ, false, hello.say());
  TEST_STDOUT_END("Hello World! : \n");
}

TEST_F(HelloTest, Tell) {
  TEST_IN(const char *TEST_STRING = "TEST"; Hello hello(TEST_STRING););
  TEST_OUT(EXPECT_STREQ, TEST_STRING, ACCESS(hello, Hello, tell)());
}

TEST_F(HelloTest, TellP) {
  TEST_IN(const char *TEST_STRING = "TEST"; Hello *hello_p = new Hello(TEST_STRING););
  TEST_OUT(EXPECT_STREQ, TEST_STRING, ACCESS_P(hello_p, Hello, tell)());
  delete hello_p;
}

TEST_F(HelloTest, listen) {
  TEST_STDIN(TEST_STRING);
  TEST_IN(Hello hello(TEST_STRING););
  TEST_OUT(EXPECT_EQ, TEST_STRING, hello.listen());
}

} // namespace

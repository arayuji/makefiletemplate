#include "channel.h"

#include "test_utils.h"

using StringChannel = Channel<std::string>;

REVEAL(StringChannel, queue_);

namespace {

class ChannelTest : public TestTemplate {
protected:
  const std::string TEST_STRING = "test";
};

TEST_F(ChannelTest, enqueue) {
  TEST_IN(StringChannel channel;);
  channel.enqueue(TEST_STRING);
  TEST_OUT(EXPECT_EQ, TEST_STRING,
           ACCESS(channel, StringChannel, queue_).front());
}

TEST_F(ChannelTest, wait) {
  TEST_IN(StringChannel channel;
          ACCESS(channel, StringChannel, queue_).push(TEST_STRING));
  TEST_RET(EXPECT_EQ, TEST_STRING, channel.dequeue());
}

} // namespace

#include "enum.h"

#include "test_utils.h"

TEST(EnumTest, toString) {
  Enum a = Enum::A;
  EXPECT_EQ(toString(a), "A");
}

TEST(EnumTest, fromString) {
  EXPECT_EQ(fromString<Enum>("A"), Enum::A);
}


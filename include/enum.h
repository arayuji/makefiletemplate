#include "enum_utils.h"

enum class Enum {
  A,
  B,
  C
};

template<>
std::map<Enum, std::string> *enum_map<Enum>() {
  static std::map<Enum, std::string> enum_map_ = {
  ENUM_PAIR(Enum, A),
  ENUM_PAIR(Enum, B),
  ENUM_PAIR(Enum, C)
};
return &enum_map_;
}


#ifndef ABSTRUCT_SOCKET_SERVER_H_
#define ABSTRUCT_SOCKET_SERVER_H_

#include "abstruct_socket.h"

class AbstructSocketServer {
  public:
    AbstructSocketServer(const AbstructSocketServer&) = delete;
    AbstructSocketServer& operator=(const AbstructSocketServer&) = delete;
    virtual ~AbstructSocketServer();
    virtual bool init(const std::string &service_name, int max_connections);
    template<typename F, typename... Args> auto standby(F&& func, Args &&... args) -> decltype(auto);
  private:
    const Socket kNullSocket = -1;
    Socket server_sock_ = kNullSocket;
};

template<typename F, typename... Args> auto AbstructSocketServer::standby(F&& func, Args &&... args) -> decltype(auto) {
  struct sockaddr_in client;
  socklen_t len = sizeof(client);
  Socket client_sock = accept(server_sock_, (struct sockaddr *)&client, &len);
  return func(client_sock, args...);
}

#endif


#ifndef ABSTRUCT_YOU_H_
#define ABSTRUCT_YOU_H_

#include <string>

class AbstructYou {
  public:
    virtual ~AbstructYou() {};
    virtual bool say(const std::string &str) const = 0;
    virtual void say() = 0;
};

#endif


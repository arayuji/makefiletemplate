#ifndef USER_COMMUNICATOR_H_
#define USER_COMMUNICATOR_H_

#include <memory>
#include <string>

#include "channel.h"

class UserCommunicator {
public:
  UserCommunicator();
  ~UserCommunicator();
  UserCommunicator(const UserCommunicator &) = delete;
  UserCommunicator &operator=(const UserCommunicator &) = delete;

  bool processInput();
  bool printOutput();
  void setInputChannel(std::shared_ptr<Channel<std::string>> input_channel);
  void setOutputChannel(std::shared_ptr<Channel<std::string>> output_channel);

private:
  std::shared_ptr<Channel<std::string>> input_channel_;
  std::shared_ptr<Channel<std::string>> output_channel_;

};

#endif

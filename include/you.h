#ifndef YOU_H_
#define YOU_H_

#include "abstruct_you.h"

#include <gmock/gmock.h>

class You : public AbstructYou {
 public:
  MOCK_CONST_METHOD1(say,
      bool(const std::string &str));
  MOCK_METHOD0(say,
      void());
};

#endif


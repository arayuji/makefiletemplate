#include <thread>

template <typename Func, typename... Args>
void doForever(Func f, Args... args) {
  while (std::bind(f, args...)()) {
  }
}

template <typename Func, typename... Args>
std::thread createForeverThread(Func f, Args... args) {
  return std::move(std::thread{doForever<Func, Args...>, f, args...});
}


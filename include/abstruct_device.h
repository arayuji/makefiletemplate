#ifndef ABSTRUCT_DEVICE_H_
#define ABSTRUCT_DEVICE_H_

#include "channel.h"
#include <chrono>
#include <condition_variable>
#include <memory>
#include <mutex>

enum class DeviceState {
  Executing,
  Completed,
  Canceled,
};

class DeviceStatus {
public:
  explicit DeviceStatus(DeviceState device_state, int page_number = 0);
  DeviceState getDeviceState() const;
  int getPageNumber() const;
private:
  DeviceState device_state_;
  int page_number_;
};

class AbstructDevice {
private:
  const int duration = 10;
  bool canceled_ = false;
  std::mutex mutex_;
  std::condition_variable condition_canceled_;
  std::shared_ptr<Channel<DeviceStatus>> channel_;

public:
  AbstructDevice(std::shared_ptr<Channel<DeviceStatus>> channel);
  void run(int page_number, std::ios *ios);
  void cancel();
};

#endif


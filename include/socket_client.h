#ifndef SOCKET_CLIENT_H_
#define SOCKET_CLIENT_H_

#include <memory>
#include <string>

#include "channel.h"

class SocketException : public std::runtime_error {
  public:
    using std::runtime_error::runtime_error;
};

using Socket = int;

class SocketClient {
public:
  SocketClient();
  ~SocketClient();
  SocketClient(const SocketClient &) = delete;
  SocketClient &operator=(const SocketClient &) = delete;

  bool init(const std::string &ip_address, const std::string &service_name);
  bool send();
  bool receive();
  void setInputChannel(std::shared_ptr<Channel<std::string>> input_channel);
  void setOutputChannel(std::shared_ptr<Channel<std::string>> output_channel);

private:
  const Socket NULL_SOCKET = -1;
  Socket server_socket_ = NULL_SOCKET;
  std::shared_ptr<Channel<std::string>> input_channel_;
  std::shared_ptr<Channel<std::string>> output_channel_;

  std::string readString(int fd);
  void writeString(int fd, const std::string &string);
};

#endif

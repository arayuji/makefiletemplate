#include <string>

class ClientMain {
public:
  ClientMain(int argc, char *argv[]);
  ~ClientMain();
  ClientMain(const ClientMain &) = delete;
  ClientMain &operator=(const ClientMain &) = delete;

  void run();

private:
  void parseArgs(int argc, char *argv[]);

  std::string host;
  std::string port;
};

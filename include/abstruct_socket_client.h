#ifndef ABSTRUCT_SOCKET_CLIENT_H_
#define ABSTRUCT_SOCKET_CLIENT_H_

#include "abstruct_socket.h"

class AbstructSocketClient {
  public:
    AbstructSocketClient(const AbstructSocketClient&) = delete;
    AbstructSocketClient& operator=(const AbstructSocketClient&) = delete;
    virtual ~AbstructSocketClient();
    virtual bool init(const std::string &ip_address, const std::string &service_name);
    template<typename F, typename... Args> auto standby(F&& func, Args &&... args) -> decltype(auto);
  private:
    const Socket kNullSocket = -1;
    Socket server_sock_ = kNullSocket;
};

template<typename F, typename... Args> auto AbstructSocketClient::standby(F&& func, Args &&... args) -> decltype(auto) {
  struct sockaddr_in client;
  socklen_t len = sizeof(client);
  Socket client_sock = accept(server_sock_, (struct sockaddr *)&client, &len);
  return func(client_sock, args...);
}

#endif


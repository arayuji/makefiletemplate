#ifndef ABSTRUCT_SOCKET_H_
#define ABSTRUCT_SOCKET_H_

#include <string>
#include <stdexcept>
#include <sys/socket.h>

class SocketException : public std::runtime_error {
  public:
    using std::runtime_error::runtime_error;
};

using Socket = int;

#endif


#include <algorithm>
#include <map>
#include <stdexcept>
#include <string>

#define ENUM_PAIR(ENUM, NAME) {ENUM::NAME, #NAME}

template <typename T> std::map<T, std::string> *enum_map() {
  static std::map<T, std::string> enum_map_;
  return &enum_map_;
}

template <typename T> std::string toString(const T &t) { return enum_map<T>()->at(t); }

template <typename T> T fromString(const std::string &string) {
  const auto map = *enum_map<T>();
  const auto itr =
      std::find_if(std::cbegin(map), std::cend(map),
                   [&string](const std::pair<T, std::string> &pair) {
                     return pair.second == string;
                   });
  if (itr == std::cend(map)) {
    throw std::invalid_argument("Unknown enum name");
  }
  return itr->first;
}

#ifndef CHANNEL_H_
#define CHANNEL_H_

#include <condition_variable>
#include <queue>
#include <mutex>

template<typename T>
class Channel {
private:
  std::queue<T> queue_;
  std::mutex mutex_;
  std::condition_variable condition_notified_;

public:
  T dequeue();
  void enqueue(const T &message);
};

#include "channel_detail.h"

#endif


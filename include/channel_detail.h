template<typename T>
T Channel<T>::dequeue() {
    std::unique_lock<std::mutex> lock(mutex_);
    condition_notified_.wait(lock, [&] { return !queue_.empty(); });
    auto data = queue_.front();
    queue_.pop();
    return data;
}

template<typename T>
void Channel<T>::enqueue(const T &message) {
    std::unique_lock<std::mutex> lock(mutex_);
    queue_.push(message);
    condition_notified_.notify_all();
}


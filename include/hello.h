#ifndef HELLO_H_
#define HELLO_H_

#include <string>
#include "you.h"

#ifndef NOLOG
#include <glog/logging.h>
#else
#define LOG Nullout
#define INFO
#define ERROR

class Nullout {
public:
	Nullout &operator<<(const std::string &) {
		return *this;
	}
};
#endif

class Hello {
public:
	Hello(const std::string &string);
	/**
	@brief Simply say hello.
	Hello says hello with the inner string.
	@return true if the string is empty
	*/
	bool say();
	bool say(const You &you) const;
  std::string listen();
protected:
	std::string m_string;
private:
	const char *tell();
	void talk();
};

#endif


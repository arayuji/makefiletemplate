#include <cstdlib>
#include <exception>
#include <iostream>

#include <chrono>
#include <condition_variable>
#include <future>
#include <initializer_list>
#include <mutex>
#include <regex>
#include <thread>
#include <vector>

#define OMP_PARALLEL_FOR _Pragma("omp parallel for")
#define DEBUG_LOG(fmt, ...) std::printf(fmt, __VA_ARGS__)

using Vector = std::vector<int>;

inline std::string operator""_s(const char *str, std::size_t length) {
  return std::string(str, length);
}

enum class Options : int {
  Default,
  Special,
};

class Class {
 public:
  Class() : Class(4){};
  virtual ~Class() = default;
  Class(const Class &) = delete;
  Class(int &&n) : number(n){};
  int operator()(int seed) {
    //スレッドセーフな変数
    thread_local int count = seed;
    //スレッドセーフな書き込み
    std::cout << std::to_string(number) + " @ " + std::to_string(count++) +
                     "\n";
    std::cout << std::to_string(number) + " @ " + std::to_string(count++) +
                     "\n";
    return count;
  }
  [[noreturn]] void escape() { std::quick_exit(EXIT_SUCCESS); }
  explicit operator int() const noexcept { return number; }
  static constexpr auto talk() noexcept { return 0; }
  virtual std::string say() & { return __func__ + " &"_s; }
  virtual std::string say() && { return __func__ + " &&"_s; }

 protected:
  int number = 0;
  std::condition_variable condition;
};

class Derived final : public Class {
 public:
  using Class::Class;
  void operator()(std::promise<int> promise) { promise.set_value(0); }
  std::string say() & override {
    return "Derived"_s;
    ;
  }
};

namespace {
template <class... Args>
struct is_callable_impl {
  // SFINAE
  template <class F>
  static std::true_type check(
      decltype(std::declval<F>()(std::declval<Args>()...), (void)0) *);
  template <class F>
  static std::false_type check(...);
};

template <class F, class... Args>
struct is_callable
    : decltype(is_callable_impl<Args...>::template check<F>(nullptr)) {};

template <class Func, class T>
[[deprecated(R"(Use "raw calling" instead)")]] auto trace(Func f = [] {},
                                                          T t = T())
    -> decltype(auto) {
  static_assert(is_callable<decltype(f), T>::value, "f is callable with t");
  return f(t);
}
}

template <typename T>
constexpr T size_template = T(0b1101'0010);

int try_cxx14() {
  static_assert(std::is_same<std::underlying_type<Options>::type, int>::value,
                "base type is int");

  //変数テンプレート
  const auto size = size_template<unsigned int>;
  static_assert(size < std::numeric_limits<decltype(size)>::max(),
                "Too big const");

  //実行時配列サイズ
  int array[size] = {9};

  //一般化キャプチャ
  auto func = [&size, ar = array ](const auto &x, const auto &y)->auto {
    return ar[x] + y;
  };
  auto bind_func = std::bind(func, size / 2, std::placeholders::_1);
  std::cout << bind_func(7) << std::endl;

  //スマートポインタ
  auto origin = std::make_unique<Class>(std::move(size));
  std::mem_fn (&Class::operator())(origin, 5);
  std::cout << origin->say() << std::endl;
  std::regex regex(R"(^[^&]*&&[^&]$)");
  std::cout << std::boolalpha << std::regex_match(Class{}.say(), regex)
            << std::endl;

  //ムーブセマンティクス
  auto instance = std::move(origin);

  // std::threadは返り値を捨てる、例外を親で処理するにはstd::exception_ptr
  try {
    auto begin = std::chrono::steady_clock::now();
    std::thread thread(std::ref(*instance), 3);
    std::cout << "id: " << thread.get_id() << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    thread.join();
    auto end = std::chrono::steady_clock::now();
    std::cout
        << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count()
        << std::endl;
  } catch (std::exception &exception) {
    std::cout << exception.what() << std::endl;
  }

  // std::promise
  try {
    std::promise<int> promise;
    auto future = promise.get_future();
    std::thread thread(std::ref(*std::make_unique<Derived>()),
                       std::move(promise));
    std::cout << "promise: " << future.get() << std::endl;
    thread.join();
  } catch (std::exception &exception) {
    std::cout << exception.what() << std::endl;
  }

  // std::packaged_task
  try {
    std::packaged_task<int(int)> task(std::ref(*instance));
    auto future = task.get_future();
    std::thread thread(std::move(task), 3);
    thread.detach();
    std::cout << "packaged_task: " << future.get() << std::endl;
  } catch (std::exception &exception) {
    std::cout << exception.what() << std::endl;
  }

  // std::async
  try {
    auto future = std::async(std::launch::async, std::ref(*instance), 3);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    std::cout << "async: " << future.get() << std::endl;
  } catch (std::exception &exception) {
    std::cout << exception.what() << std::endl;
  }
  return EXIT_SUCCESS;
}
